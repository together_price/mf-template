import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { useEffect } from 'react';
import en from './en/translation.json';
import es from './es/translation.json';
import it from './it/translation.json';

const instance = i18n.createInstance().use(initReactI18next);

export const useChangeLocale = (): void => {
  useEffect(() => {
    const onLocaleChange = ({ detail: language }: CustomEvent) => instance.changeLanguage(language);
    window.addEventListener('change-locale', onLocaleChange);
    return () => window.removeEventListener('change-locale', onLocaleChange);
  });
};

export const useLocale = (currentLanguage: string) => {
  useEffect(() => {
    instance.changeLanguage(currentLanguage);
  }, [currentLanguage]);
  useChangeLocale();
};

instance.init({
  resources: {
    en: {
      translation: en,
    },
    es: {
      translation: es,
    },
    it: {
      translation: it,
    },
  },
  lng: 'en',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
});

export default instance;
