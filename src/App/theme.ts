import { createMuiTheme } from '@material-ui/core';
import { CSSProperties } from '@material-ui/core/styles/withStyles';

const defaultTheme = createMuiTheme();
const responsiveFontSize = (lg: string, md: string): CSSProperties => ({
  fontSize: lg,
  [defaultTheme.breakpoints.down('md')]: {
    fontSize: md,
  },
});
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#6A3EEA',
      dark: '#17278D',
      light: '#EAE8FE',
      contrastText: '#F9F9FC',
    },
    secondary: {
      main: '#FA5D75',
      contrastText: '#F9F9FC',
    },
    warning: {
      main: '#FEA41D',
      contrastText: '#F9F9FC',
    },
    type: 'light',
  },
  typography: {
    h1: {
      fontFamily: 'Poppins',
      fontWeight: 600,
      ...responsiveFontSize('28px', '21px'),
    },
    h2: {
      fontFamily: 'Poppins',
      fontWeight: 600,
      ...responsiveFontSize('26px', '18px'),
    },
    subtitle1: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      ...responsiveFontSize('24px', '16px'),
    },
    subtitle2: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      ...responsiveFontSize('20px', '14px'),
    },
    body1: {
      fontFamily: 'Poppins',
      fontWeight: 400,
      ...responsiveFontSize('20px', '14px'),
    },
    body2: {
      fontFamily: 'Poppins',
      fontWeight: 400,
      ...responsiveFontSize('16px', '13px'),
    },
    caption: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      ...responsiveFontSize('16px', '13px'),
    },
    overline: {
      fontFamily: 'Poppins',
      fontWeight: 400,
      ...responsiveFontSize('16px', '14px'),
    },
    button: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      fontSize: '16px',
    },
  },
  overrides: {
    MuiButton: {
      sizeLarge: {
        height: '50px',
      },
    },
    MuiCssBaseline: {
      '@global': {
        '@font-face': {
          fontFamily: 'Poppins',
        },
      },
    },
  },
});

export default theme;
