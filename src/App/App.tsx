import React, { Suspense } from 'react';
import ScopedCssBaseline from '@material-ui/core/ScopedCssBaseline';
import {
  StylesProvider,
  createGenerateClassName,
  ThemeProvider,
} from '@material-ui/core/styles';
import { I18nextProvider } from 'react-i18next';
import i18n, { useLocale } from '../i18n';
import theme from './theme';
import Routes from './Routes';
import AppContext, { AppContextValue } from './AppContext';

export type AppProps = AppContextValue;
const App: React.FC<AppProps> = ({
  name,
  lng,
  basename,
}) => {
  useLocale(lng);
  return (
    <AppContext.Provider value={{ name, lng, basename }}>
      <I18nextProvider i18n={i18n}>
        <ScopedCssBaseline>
          <StylesProvider
            generateClassName={createGenerateClassName({
              seed: name,
            })}
          >
            <ThemeProvider theme={theme}>
              <Suspense fallback="Loading...">
                <Routes />
              </Suspense>
            </ThemeProvider>
          </StylesProvider>
        </ScopedCssBaseline>
      </I18nextProvider>
    </AppContext.Provider>
  );
};

export default App;
