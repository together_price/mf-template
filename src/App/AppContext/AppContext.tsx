import React, { useContext } from 'react';

export type AppContextValue = {
  name: string,
  lng: string,
  basename?: string,
};
const AppContext = React.createContext<AppContextValue>(null);

export const useApp = (): AppContextValue => useContext(AppContext);

export default AppContext;
