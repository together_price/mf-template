import React from 'react';
import { Trans } from 'react-i18next';
import { Box, Container, Typography } from '@material-ui/core';
import { useApp } from '../../AppContext/AppContext';

export type HomeProps = {};
const Home: React.FC<HomeProps> = (props) => {
  const { name } = useApp();
  return (
    <>
      <Container>
        <Box
          display="flex"
          alignItems="center"
          textAlign="center"
          p={1}
          m={1}
          css={{ height: 'calc(100vh - 16px)', width: '100%' }}
        >
          <Container maxWidth="md">
            <Typography variant="h1">
              <Trans i18nKey="hello-world">
                Hello
                {{ name }}
              </Trans>
            </Typography>
          </Container>
        </Box>
      </Container>
    </>
  );
};

export default Home;
