import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import React from 'react';
import { useApp } from '../AppContext/AppContext';

const Home = React.lazy(() => import('./Home'));

export type RoutesProps = {};
const Routes: React.FC<RoutesProps> = () => {
  const { basename } = useApp();
  return (
    <>
      <Router basename={basename}>
        <Switch>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </>
  );
};

export default Routes;
