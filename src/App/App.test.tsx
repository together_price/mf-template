import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

describe('App component', () => {
  it('should be rendered without error', () => {
    expect(() => render(<App name="mf-template" lng="en" />)).not.toThrowError();
  });
});
