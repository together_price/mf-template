# together-price/mf-template

Together price micro-frontend.

## Start standalone

Start the micro-frontend for development.

```bash
npm run standalone
```

Visit the url http://localhost:3000/.

## Login

When started in the standalone mode, 
it's possible to login using the url http://localhost:3000/login?email=example@getnada.com&password=12345678

## Rules

- all code must follow the [airbnb/javascript](https://github.com/airbnb/javascript) style guide
- class components must be avoided, **functional** components and **hooks** must be preferred
- all components must be written in [typescript](https://www.typescriptlang.org/)
- all components must be written using [@material-ui](https://material-ui.com/) components
- all components must export the `props` type

```typescript
    export type HelloWorldProps = { /**...*/ };
    const HelloWorld: React.FC<HelloWorldProps> = (props) => /**...*/;
    export default HelloWorld;
```
  
- all styles must be written using the [@material-ui/styles](https://material-ui.com/styles/basics/) approach
- global states must be avoided, if necessary one of the above solutions must be used:

    - [react context](https://it.reactjs.org/docs/context.html)
    - [recoil](https://recoiljs.org/) 
    
- api call must be done using the provided [api service](TODO add link)
- api call must be cached, if necessary, using the library [swr](https://github.com/vercel/swr)
- i18n must be implemented using [i18n-next](https://react.i18next.com/)
- each component must have its own file called like the component it exports
- component file name must use the pascal-case convention
- one component file must export only one component as default export
- component with other resources other than the component file,
    must be inside a directory with the name of the component.
    This directory must also have an index.tsx exporting the component as default exports.
    All the resources (or tests) file must be inside this directory.
    
```markdown
  // Example HelloWorld component
  
  ├── HelloWorld
  │   ├── index.tsx
  │   ├── HelloWorld.tsx
  │   ├── HelloWorld.stories.tsx
  │   ├── HelloWorld.spec.tsx
  │   ├── assets
  └── └── └── logo.png
```
- non-component files name must follow the kebab-case naming convention

## Resources

- [airbnb/javascript](https://github.com/airbnb/javascript)
- [@material-ui](https://material-ui.com/)
- [@material-ui/styles](https://material-ui.com/styles/basics/)
- [swr](https://github.com/vercel/swr)
- [i18n-next](https://react.i18next.com/)
- [react context](https://it.reactjs.org/docs/context.html)
- [recoil](https://recoiljs.org/) 

## Clone

Clone this template using [sauron](https://github.com/alfredosalzillo/sauron).

```bash
sauron init mf-hello-world --template https://bitbucket.org/together_price/mf-template.git
```

---
Using [airbnb/javascript](https://github.com/airbnb/javascript) style guide.
