node {
    def nodeImage
    def applicationName = "together-price-mf-template"
    def envToDeploy = "${env.BRANCH_NAME}"
    if(env.BRANCH_NAME == 'master') envToDeploy = "prod"
    if(envToDeploy != 'dev' && envToDeploy != 'staging' && envToDeploy != 'prod') {
        currentBuild.result = 'SUCCESS'
        return
    }

    def nodeContainer = docker.image("node")
    properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '10'))])

    stage('Clone repository') {
        checkout scm
    }

    def lastSuccessfulCommit = getLastSuccessfulCommit()
    def currentCommit = commitHashForBuild(currentBuild.rawBuild)
    if (lastSuccessfulCommit) {
        def commits = sh(
            script: "git rev-list $currentCommit \"^$lastSuccessfulCommit\"",
            returnStdout: true
        ).split('\n')
        if(commits.findAll { it != null && !it.equals("") }.size() == 0) {
            currentBuild.result = 'SUCCESS'
            return
        }
    }

    withCredentials([
        string(credentialsId: "jwt_${envToDeploy}", variable: 'jwtToken'),
        string(credentialsId: "web_env_base64_${envToDeploy}", variable: 'envBase64')
        string(credentialsId: "aws_org_id", variable: 'orgId')
    ]) {
        stage('Build image') {
            nodeImage = docker.build("${applicationName}", "--build-arg APP_ENV=${envToDeploy} --build-arg APP_VERSION=2.${env.BUILD_NUMBER} --build-arg APP_BASE64_ENV=${envBase64} -f ./docker/Dockerfile .")
        }

        stage('Push image') {
                sh 'rm  ~/.dockercfg || true'
                sh 'rm ~/.docker/config.json || true'
                docker.withRegistry("https://${orgId}.dkr.ecr.eu-west-1.amazonaws.com/${applicationName}", 'ecr:eu-west-1:ecr-credential') {
                    nodeImage.push("${envToDeploy}-${currentCommit}")
                    nodeImage.push("latest")
                }
            }

            stage('Deploy') {
                 if (envToDeploy != 'prod') {
                     try {
                         echo 'Deploy on ${envToDeploy}'
                         docker.withRegistry("https://${orgId}.dkr.ecr.eu-west-1.amazonaws.com/${applicationName}", 'ecr:eu-west-1:ecr-credential') {
                             docker.image("${applicationName}:${envToDeploy}-${currentCommit}").inside("-v /root/.aws/credentials:/root/.aws/credentials") {c ->
                                sh script: "cd /usr/src/app; npm run sync:${envToDeploy}", label: 'Sync S3 Bucket'
                             }
                         }
                         slackSend (channel: "#devops", color: '#4286f4', message: "Deploy completed of branch ${env.BRANCH_NAME}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.JOB_DISPLAY_URL})")
                     } catch(Exception e) {
                         slackSend (channel: "#devops", color: '#f49241', message: "Deploy failed of branch ${env.BRANCH_NAME}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.JOB_DISPLAY_URL})")
                         throw e;
                     }
                 }
                 if (envToDeploy == 'prod') {
                      timeout(time: 2, unit: "HOURS") {
                          input message: 'Do you approve the deploy of together-price-web on production environment?', ok: 'Yes'
                          try {
                              echo 'Deploy on prod'
                              docker.withRegistry("https://${orgId}.dkr.ecr.eu-west-1.amazonaws.com/${applicationName}", 'ecr:eu-west-1:ecr-credential') {
                                   docker.image("${applicationName}:${envToDeploy}-${currentCommit}").inside("-v /root/.aws/credentials:/root/.aws/credentials") {c ->
                                      sh script: "cd /usr/src/app; npm run sync:${envToDeploy}", label: 'Sync S3 Bucket'
                                   }
                              }
                              slackSend (channel: "#devops", color: '#4286f4', message: "Deploy completed on PROD of branch ${env.BRANCH_NAME}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.JOB_DISPLAY_URL})")
                          } catch(Exception e) {
                              slackSend (channel: "#devops", color: '#f49241', message: "Deploy failed on PROD of branch ${env.BRANCH_NAME}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.JOB_DISPLAY_URL})")
                              throw e;
                          }
                      }
                  }
            }
    }
}

def getLastSuccessfulCommit() {
    def lastSuccessfulHash = null
    def lastSuccessfulBuild = currentBuild.rawBuild.getPreviousSuccessfulBuild()
    if ( lastSuccessfulBuild ) {
        lastSuccessfulHash = commitHashForBuild( lastSuccessfulBuild )
    }
    return lastSuccessfulHash
}

/**
 * Gets the commit hash from a Jenkins build object, if any
 */
@NonCPS
def commitHashForBuild( build ) {
    def scmAction = build?.actions.find { action -> action instanceof jenkins.scm.api.SCMRevisionAction }
    return scmAction?.revision?.hash
}
