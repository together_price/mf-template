const webpackMerge = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");
const Dotenv = require("dotenv-webpack");
const HtmlWebpackTagsPlugin = require("html-webpack-tags-plugin");
const HtmlInsertTagWebpackPlugin = require('html-insert-tag-webpack-plugin')

// TODO move this configuration into an external library

const loginScript = `
   System.import('single-spa').then(function (singleSpa) {
        console.log('together-price-standalone-login registered')
        console.log('together-price-standalone-login usage: navigate to /login?email=example@getnada.com&password=12345678')
        if (localStorage.AuthStore) console.log('together-price-standalone-login already logged in');
        singleSpa.registerApplication({
          name: 'together-price-standalone-login',
          app: function () {
            return Promise.resolve({
              bootstrap() {
                console.log('together-price-standalone-login bootstrapped')
                return Promise.resolve();
              },
              async mount() {
                 const params = new URLSearchParams(location.search);
                 const email = params.get('email');
                 const password = params.get('password');
                 if (!email || !password) {
                  console.error('together-price-standalone-login missing credentials');
                  throw new Error("missing credentials");
                 }
                 console.log('together-price-standalone-login login using credentials', { email, password });
                 return fetch('https://apiv2.dev.togetherprice.com/tp-ums/ums/auth', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ email, password })
                 }).then(async (response) => {
                    if (response.ok) {
                      return response.json();
                    }
                    throw await response.json();
                 }).then((data) => { 
                    localStorage.setItem('AuthStore', JSON.stringify(data));
                    location.href = '/';
                 }).catch((e) => {
                    console.error('together-price-standalone-login error during login', e);
                    throw e;
                 });
              },
              unmount() {
                return Promise.resolve();
              }
            });
          },
          activeWhen: '/login'
        });
   });
`;

const imageLoader = (env) => ({
  test: /\.(png|svg|jpg|jpeg|gif)$/i,
  use: [
    {
      loader: "file-loader",
      options: {
        publicPath: env.publicPath,
      },
    },
  ],
});
const togetherPriceDefault = (projectName, env, argv) => {
  const config = singleSpaDefaults({
    orgName: "together-price",
    projectName,
    webpackConfigEnv: env,
    argv,
  });
  if (!env.standalone) {
    config.externals.push(
      "@material-ui/core",
      "@material-ui/icons",
      "@material-ui/lab",
      "react-router-dom",
      "swr",
      "recoil",
      "react-i18next",
      "i18next",
    );
  }
  return webpackMerge.smart(config, {
    module: {
      rules: [
        imageLoader(env),
      ],
    },
    plugins: [
      new Dotenv({
        path: "./.env", // Path to .env file (this is the default)
        safe: false, // load .env.example (defaults to "false" which does not use dotenv-safe)
      }),
      ...(env.standalone
        ? [
          new HtmlWebpackTagsPlugin({
            links: [
              {
                path: "https://fonts.gstatic.com",
                attributes: { rel: "preconnect" },
              },
              {
                path:
                  "https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap",
              },
            ],
          }),
          new HtmlInsertTagWebpackPlugin([{
            tagName: 'script',
            innerHTML: loginScript,
            inject: {
              tagName: 'body',
              location: 'after'
            }
          }])
        ]
        : []),
    ],
  });
};

module.exports = togetherPriceDefault;
