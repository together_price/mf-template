const webpackMerge = require("webpack-merge");
const togetherPriceDefault = require("./webpack-config-together-price-single-spa-react-ts");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = togetherPriceDefault(
    "{{PROJECT_NAME}}",
    webpackConfigEnv,
    argv
  );

  return webpackMerge.smart(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
  });
};
